<?php

/**
 * Hello Word route example by Get . 
 */

Route::get('/helloword', function(){
    return view('gasso::hola');
});

Route::group(['prefix'=>'api','as'=>'api'], function(){
    
    Route::get('/helloword', function(){

        return response()->json([
            'message'=> 'HelloWord'
        ],200);
    });
});
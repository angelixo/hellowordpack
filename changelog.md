# Changelog

All notable changes to `holamundo` will be documented in this file.

## Version 2.5
- Now Api response is a `jsson` with a `message` (hello word) and 200. 

## Version 2.0
###Removed
-Dependencies 

## Version 1.0
###Added
- It says Hello Word!

###Removed
- Contributions.md

## Version 0.0.1

### Added
- Everything

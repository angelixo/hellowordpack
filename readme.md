# Holamundo

HelloWord template of a Laravel 5.7 package.

## Installation

Via Composer

``` bash
$ composer require gasso/holamundo
```

## Usage

Just navigate to `/helloword` or `/api/helloword`

## Change log

Please see the [changelog](changelog.md) for more information on what has changed recently.

## Testing

``` bash
$ composer test
```
## Security

If you discover any security related issues, please email angelixo@gmail.com instead of using the issue tracker.

## Credits

- angelixo[angelixo@gmail.com]


## License

MIT. Please see the [license file](license.md) for more information.

[ico-version]: https://img.shields.io/packagist/v/gasso/holamundo.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/gasso/holamundo.svg?style=flat-square
[ico-travis]: https://img.shields.io/travis/gasso/holamundo/master.svg?style=flat-square
[ico-styleci]: https://styleci.io/repos/12345678/shield

[link-packagist]: https://packagist.org/packages/gasso/holamundo
[link-downloads]: https://packagist.org/packages/gasso/holamundo
[link-travis]: https://travis-ci.org/gasso/holamundo
[link-styleci]: https://styleci.io/repos/12345678
[link-author]: https://github.com/gasso
[link-contributors]: ../../contributors
